# README #

Custom Odoo Dashboard. Study Case Durian Surabaya.

### What is this repository for? ###

* This is custom dashboard for odoo to display top contributor, top product, and top buyer from the respective product
* Version 0.1 at the moment
* Visualization using Highcharts (http://www.highcharts.com/)


### How do I get set up? ###

* Run using php with postgre function enabled
* Configure the connection method to connect to the database in conf/conn.php
* Configure on your own using the index.php

### Who do I talk to? ###

* Fork Request only accepted by me
* Feel free to fork request and create brach

Author: Valliant Ferlyando