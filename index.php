<!DOCTYPE html>
<html lang="en">
  <head>
  
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript" src="js/jquery.min.js"></script>

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Durian Surabaya Report</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

		</style>
		<script type="text/javascript">
$(function () {
    // Set up the chart
    var chart;
	$('document').ready(function() {
	 $.getJSON("json/r1.php", function(json) {
                  $.getJSON("json/r2.php", function(json2){
	chart	= new Highcharts.Chart({
        chart: {
            renderTo: 'overall',
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 50,
                viewDistance: 25
				
            }
        },
        title: {
            text: 'Chart Top Buyer Consumer Durian Surabaya'
        },
        subtitle: {
            text: 'Grafik ini menjelaskan kustomer dengan pembelian paling banyak'
        },
		xAxis: {
                        categories: json
                    },
		
        plotOptions: {
            column: {
                depth: 25
            }
        },
        series: [{
		name:'Total Item',
            data: json2,color: '#00FF00'
        }]
    });

    function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }

    // Activate the sliders
    $('#sliders input').on('input change', function () {
        chart.options.chart.options3d[this.id] = this.value;
        showValues();
        chart.redraw(false);
    });

    showValues();
});
});
});
});
		</script>
		
		
		<script type="text/javascript">
$(function () {
    // Set up the chart
    var chart;
	$('document').ready(function() {
	 $.getJSON("json/r3.php", function(json) {
                  $.getJSON("json/r4.php", function(json2){
	chart	= new Highcharts.Chart({
        chart: {
            renderTo: 'overall2',
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 50,
                viewDistance: 25
            }
        },
        title: {
            text: 'Chart Top Buyer Consumer Durian Surabaya'
        },
        subtitle: {
            text: 'Grafik ini menjelaskan kustomer dengan pembelian paling banyak'
        },
		xAxis: {
                        categories: json
                    },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        series: [{
		name:'Total Money Spent',
            data: json2
        }]
    });

    function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }

    // Activate the sliders
    $('#sliders input').on('input change', function () {
        chart.options.chart.options3d[this.id] = this.value;
        showValues();
        chart.redraw(false);
    });

    showValues();
});
});
});
});
		</script>
		
<script type="text/javascript">
$(function () {
    // Set up the chart
    var chart;
	$('document').ready(function() {
	 $.getJSON("json/r5.php", function(json) {
                  $.getJSON("json/r6.php", function(json2){
	chart	= new Highcharts.Chart({
        chart: {
            renderTo: 'overall3',
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 50,
                viewDistance: 25
            },
			backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                    [0, 'rgb(200, 200, 255)'],
                    [1, 'rgb(255, 255, 255)']
                ]
            }
        },
        title: {
            text: 'Chart Top Item Durian Surabaya'
        },
        subtitle: {
            text: 'Grafik ini menjelaskan item yang paling banyak dibeli'
        },
		xAxis: {
                        categories: json
                    },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        series: [{
		name:'Total Item Sold',
            data: json2,color: {
    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 0},
    stops: [
        [0, 'rgb(255, 255, 255)'],
        [1, '#3366AA']
    ]
}
        }]
    });

    function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }

    // Activate the sliders
    $('#sliders input').on('input change', function () {
        chart.options.chart.options3d[this.id] = this.value;
        showValues();
        chart.redraw(false);
    });

    showValues();
});
});
});
});
		</script>		
  </head>
  <body>
  <script src="js/highcharts.js"></script>
<script src="js/highcharts-3d.js"></script>
<script src="js/modules/exporting.js"></script>
	 <script type="text/javascript">
    document.getElementById("dash").setAttribute("class","active");
    </script>    
    <div class="container">
        <div class="page-header">
            <h1>Durian Surabaya Reporting Dashboard</h1>

            <ul class="nav nav-pills" role="tablist">
                <li role="presentation" class="active"><a href="#cost" aria-controls="cost" role="tab" data-toggle="tab">Top Buyer</a></li>
                <li role="presentation"><a href="#total" aria-controls="total" role="tab" data-toggle="tab">Top Contributor </a></li>
                <li role="presentation"><a href="#volume" aria-controls="volume" role="tab" data-toggle="tab">Top Product </a></li>
               
            </ul>
        </div>
    <div> </div>
    <div><h3>Report Overview</h3></div>
    </div>
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane fade in active" id="cost">
						
						<div class="jumbotron" id="overall">
						</div>
						
						<br></br>
						<h3>Data Penjualan produk</h3>
						<h4>Berikut merupakan laporan mengenai customer dengan kuantitas pembelian tertinggi</h4>
						<br></br>
						
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>Name</th>
            <th>Biaya Dibayar</th>
            <th>Tanggal Dipesan</th>
            <th>Status Pembayaran</th>
            <th>Nama Transaksi</th>
			<th>Nama Produk</th>
			<th>Kuantitas</th>
        </tr>
    </thead>
   <?php
		include('conf/conn.php');
		

      $result = pg_query($dbconn, "SELECT b.name, a.amount_untaxed ,a.date_order ,a.invoice_status,a.name,c.name ,c.qty_invoiced from sale_order as a, res_partner as b, sale_order_line as c where (b.id = a.partner_id ) and (a.invoice_status = 'invoiced') and a.id =c .order_id ORDER BY c.qty_invoiced DESC LIMIT 10")  or die('Query fail: ' . pg_last_error());
    ?>
    <tbody>
  <?php while ($row = pg_fetch_array($result)) { 
    echo "<tr>";
        echo "<td>" . $row[0] . "</td>";
        echo "<td>" . $row[1] . "</td>";
		echo "<td>" . $row[2] . "</td>";
        echo "<td>" . $row[3] . "</td>";
        echo "<td>" . $row[4] . "</td>";
		echo "<td>" . $row[5] . "</td>";
        echo "<td>" . $row[6] . "</td>";	
		echo "</tr>" ;
  }?>
</tbody>
</table>

<br></br>
						<h3></h3>
						<h4>Berikut merupakan laporan mengenai historis pembelian item urut berdasarkan kronologis</h4>
						<br></br>

<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>Name</th>
            <th>Biaya Dibayar</th>
            <th>Tanggal Dipesan</th>
            <th>Status Pembayaran</th>
            <th>Nama Transaksi</th>
			<th>Nama Produk</th>
			<th>Kuantitas</th>
        </tr>
    </thead>
   <?php
		include('conf/conn.php');
		

      $result = pg_query($dbconn, "SELECT b.name, a.amount_untaxed ,a.date_order ,a.invoice_status,a.name,c.name ,c.qty_invoiced from sale_order as a, res_partner as b, sale_order_line as c where (b.id = a.partner_id ) and (a.invoice_status = 'invoiced') and a.id =c .order_id ORDER BY a.date_order DESC LIMIT 15")  or die('Query fail: ' . pg_last_error());
    ?>
    <tbody>
  <?php while ($row = pg_fetch_array($result)) { 
    echo "<tr>";
        echo "<td>" . $row[0] . "</td>";
        echo "<td>" . $row[1] . "</td>";
		echo "<td>" . $row[2] . "</td>";
        echo "<td>" . $row[3] . "</td>";
        echo "<td>" . $row[4] . "</td>";
		echo "<td>" . $row[5] . "</td>";
        echo "<td>" . $row[6] . "</td>";	
		echo "</tr>" ;
  }?>
</tbody>
</table>
					</div>

					<div role="tabpanel" class="tab-pane fade" id="total">
						
						<div class="jumbotron" id="overall2">					
						</div>
						<br></br>
						<h3>Laporan Pembelian</h3>
						<h4>Berikut merupakan laporan mengenai customer dengan kuantitas item pembelian tertinggi dengan item favorit yang sering mereka beli</h4>
						<br></br>
						
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>Nama Pelanggan</th>
            <th>Item Paling Banyak Dibeli</th>
            <th>Kuantitas</th>
        </tr>
    </thead>
   <?php
		include('conf/conn.php');
		

      $result = pg_query($dbconn, "SELECT DISTINCT d.name,c.name,c.qty_invoiced from 
sale_order as a 
INNER JOIN 
res_partner as b 
ON b.id = a.partner_id 
INNER JOIN 
sale_order_line as c 
ON a.id =c .order_id 
INNER JOIN 
(SELECT b.name,MAX(c.qty_invoiced) as jumlah from 
sale_order as a 
INNER JOIN 
res_partner 
as b ON b.id = a.partner_id 
INNER JOIN 
sale_order_line as c 
ON a.id =c .order_id 
where (a.invoice_status = 'invoiced')
GROUP BY b.NAME
ORDER BY jumlah DESC) as d
ON d.name = b.name and d.jumlah=c.qty_invoiced
where (a.invoice_status = 'invoiced')
ORDER BY c.qty_invoiced DESC")  or die('Query fail: ' . pg_last_error());
    ?>
    <tbody>
  <?php while ($row = pg_fetch_array($result)) { 
    echo "<tr>";
        echo "<td>" . $row[0] . "</td>";
        echo "<td>" . $row[1] . "</td>";
		echo "<td>" . $row[2] . "</td>";
		echo "</tr>" ;
  }?>
</tbody>
</table>
						
									

					</div>
					<div role="tabpanel" class="tab-pane fade" id="volume">
						
						<div class="jumbotron" id="overall3"></div>

					</div>
					<div role="tabpanel" class="tab-pane fade" id="kelurahan">

						<div class="jumbotron" id="overall4"></div>
							
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div id="sliders">
							<table>
								<tr>
									<td>Alpha Angle</td>
									<td><input id="alpha" type="range" min="0" max="45" value="15"/> <span id="alpha-value" class="value"></span></td>
								</tr>
								<tr>
									<td>Beta Angle</td>
									<td><input id="beta" type="range" min="-45" max="45" value="15"/> <span id="beta-value" class="value"></span></td>
								</tr>
								<tr>
									<td>Depth</td>
									<td><input id="depth" type="range" min="20" max="100" value="50"/> <span id="depth-value" class="value"></span></td>
								</tr>
							</table>
						</div>
			</div>
		</div>
	</div>

	
	<div id="container"></div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="api/js/jquery-1.11.3.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	
  </body>
</html>